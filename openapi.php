<?php

require 'vendor/autoload.php';
$openapi = \OpenApi\Generator::scan(['./app/Http', './bootstrap', './routes']);
header('Content-Type: application/x-yaml');
echo $openapi->toYaml();
