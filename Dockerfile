# syntax=docker/dockerfile:1

FROM composer:lts as prod-deps

    WORKDIR /capp
    RUN --mount=type=bind,source=./composer.json,target=composer.json \
        --mount=type=bind,source=./composer.lock,target=composer.lock \
        --mount=type=cache,target=/tmp/cache \
        composer install --no-dev --no-interaction


FROM composer:lts as dev-deps

    WORKDIR /capp
    RUN --mount=type=bind,source=./composer.json,target=composer.json \
        --mount=type=bind,source=./composer.lock,target=composer.lock \
        --mount=type=cache,target=/tmp/cache \
        composer install --no-interaction


FROM php:8.1-apache as base

    ENV APACHE_DOCUMENT_ROOT /var/www/html/public
    #
    # TODO disable ExtendedStatus in production
    # 
    #RUN /var/www/html/scripts/docker/extended-apache2.conf >> /etc/apache2/apache2.conf 
    RUN echo "\n"\
        "# Customisation"\
        "LoadModule status_module modules/mod_status.so"\
        "<IfModule mod_status.c>"\
            "ExtendedStatus On"\
            "<Location \"/server-status\">"\
                "SetHandler server-status"\
                "Order allow,deny"\
                "Allow from all"\
            "</Location>"\
        "</IfModule>" >> /etc/apache2/apache2.conf
    RUN sed -ri -e 's!/var/www/html!${APACHE_DOCUMENT_ROOT}!g' /etc/apache2/sites-available/*.conf
    RUN sed -ri -e 's!/var/www/!${APACHE_DOCUMENT_ROOT}!g' /etc/apache2/apache2.conf /etc/apache2/conf-available/*.conf

    RUN docker-php-ext-install pdo pdo_mysql
    COPY ./ /var/www/html

    USER root

    RUN apt-get update && apt-get upgrade -y
    RUN chown -R www-data:www-data /var/www/html
    RUN find /var/www/html -type f -exec chmod 644 {} \;
    RUN find /var/www/html -type d -exec chmod 755 {} \;
    RUN a2enmod rewrite
    RUN a2enmod status


FROM base as development

    RUN apt-get install -y \
        build-essential \
        locales \
        zip \
        vim \
        unzip \
        git \
        curl \
        iputils-ping \
        lynx
    # mariadb-client net-tools dnsutils nmap
    RUN apt-get clean && rm -rf /var/lib/apt/lists/*

    RUN chmod -R ug+x /var/www/html/scripts && /var/www/html/scripts/post-root-package-install.sh
    RUN pecl install xdebug && docker-php-ext-enable xdebug
    RUN /var/www/html/scripts/container-init.sh >> /var/www/html/storage/logs/container-init.log 2>&1 &

    COPY ./tests /var/www/html/tests
    RUN mv "$PHP_INI_DIR/php.ini-development" "$PHP_INI_DIR/php.ini"
    COPY --from=dev-deps /usr/bin/composer /usr/local/bin/composer
    COPY --from=dev-deps /capp/vendor/ /var/www/html/vendor

    USER www-data


FROM base as final

    RUN apt-get clean && rm -rf /var/lib/apt/lists/*
    RUN mv "$PHP_INI_DIR/php.ini-production" "$PHP_INI_DIR/php.ini"
    COPY --from=prod-deps /capp/vendor/ /var/www/html/vendor

    USER www-data
