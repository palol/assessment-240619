<?php

namespace App\Services;

/**
 * Description of Sanitation
 */
class Sanitation
{
    public function cropInternationalCountryCode(string $input): string
    {
        $output = $input;

        return $output;
    }

    public function sanitizeString(string $input): string
    {
        $output = trim($input);

        return filter_var($output, FILTER_SANITIZE_STRING);
    }

    public function sanitizeStringArray(array $input): array
    {
        $output = [];
        foreach ($input as $key => $value) {
            $output[$key] = $this->sanitizeString($value);
        }

        return $output;
    }

    public function sanitizeItem(string $key, mixed $value): mixed
    {
        if ($key === 'telephone') {
            $value = $this->sanitizeString($value);
            $value = $this->cropInternationalCountryCode($value);
        } elseif (is_array($value)) {
            $value = $this->sanitizeStringArray($value);
        } else {
            $value = $this->sanitizeString($value);
        }

        return $value;
    }

    public function __construct() {}
}
