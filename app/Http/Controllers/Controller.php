<?php

namespace App\Http\Controllers;

use Laravel\Lumen\Routing\Controller as BaseController;

class Controller extends BaseController
{
    /**
     * @see https://code.tutsplus.com/how-to-secure-a-rest-api-with-lumen--cms-27442t
     */
    public function __construct()
    {
        $this->middleware('auth', ['only' => [
            'create',
            'update',
            'delete',
        ]]);

        $this->middleware('sanitize', ['only' => [
            'create',
            'update',
        ]]);
    }
}
