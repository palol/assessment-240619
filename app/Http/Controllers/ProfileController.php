<?php

namespace App\Http\Controllers;

//use App\Http\BaseRequest as Request;
use App\Models\ProfileAttribute;
use App\Models\Profile;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;

/**
 * @OA\Info(title="Profile CRUD API", version="0.1.5")
 */
class ProfileController extends Controller
{
    /**
     * @OA\Get(
     *     path="/api/profiles",
     *
     *     @OA\Response(response="200", description="Lists profiles.")
     * )
     *
     * Checks for all the profile resources filtering soft deleted items.
     * It paginates 15 results at a time.
     *
     * @return ?string
     */
    public function showAllProfiles(): ?string
    {
        return new JsonResponse(Profile::with('profileAttributes')->simplePaginate(), 200);
    }

    /**
     * @OA\Get(
     *     path="/api/profile/{id}",
     *
     *     @OA\Parameter(name="id", in="path", @OA\Schema(type="integer")),
     *
     *     @OA\Response(response="200", description="Checks for a single profile resource by $id with all referenced profile_attributes."
     * )
     *
     * Checks for a single profile resource by $id with all
     * referenced profile_attributes.
     */
    public function showProfileById(int $id): string
    {
        $profile = Profile::find($id);

        if ($profile !== null) {
            $profileAttributes = $profile->profileAttributes()->getResults();
            $profile->setRelation('profile_attributes', $profileAttributes);
        }

        return new JsonResponse($profile, 200);
    }

    /**
     * @OA\Post(
     *     path="/api/profile",
     *
     *     @OA\Parameter(name="first_name", in="path", @OA\Schema(type="string")),
     *     @OA\Parameter(name="last_name", in="path", @OA\Schema(type="string")),
     *     @OA\Parameter(name="telephone", in="path", @OA\Schema(type="string")),
     *     @OA\Parameter(name="profile_attributes", in="path", @OA\Schema(type="array")),
     *
     *     @OA\Response(response="201", description="Creates a new profile resource."
     * )
     *
     * Creates a new profile resource.
     */
    public function create(Request $request): string
    {
        $validated = $this->validate($request, [
            'first_name' => 'required|min:2|max:254|regex:/^([a-zA-Z]+)(\s[a-zA-Z]+)*$/',
            'last_name' => 'required|min:2|max:254|regex:/^([a-zA-Z]+)(\s[a-zA-Z]+)*$/',
            /*
            alternative
            ^(\+\d{1,2}\s?)?1?\-?\.?\s?\(?\d{3}\)?[\s.-]?\d{3}[\s.-]?\d{4}$
            */
            'telephone' => 'required|min:7|max:25|regex:/^(\+\d{1,2}\s?)?\(?\d{3}\)?[\s.-]?\d{3}[\s.-]?\d{4}$/', // Still with international prefix, source https://regex101.com/r/j48BZs/2
            'profile_attributes.*.attribute' => 'sometimes|nullable|string|max:254',
        ]);
        
        
        //$qq = $request->query();
        $profileWithoutFk = $request->except('profile_attributes');//Arr::forget($qq, 'profile_attributes');

        $profile = Profile::create($profileWithoutFk);
        
        //$profile = Profile::create($request->except('profile_attributes'));

        if (! empty($request->get('profile_attributes'))) {
            $this->createProfileAttributes($request->get('profile_attributes'), $profile->id);
        }

        return new JsonResponse($profile, 201);
    }

    /**
     * @OA\Put(
     *     path="/api/profile/{id}",
     *
     *     @OA\Parameter(name="id", in="path", @OA\Schema(type="integer")),
     *
     *     @OA\Response(response="200", description="Creates a new profile resource."
     * )
     *
     * Checks if a profile resource exists
     * and allows the resource to be updated.
     */
    public function update(int $id, Request $request): string
    {
        $profile = Profile::findOrFail($id);
        $profile->update($request->query());

        return new JsonResponse($profile, 200);
    }

    /**
     * @OA\Delete(
     *     path="/api/profile/{id}",
     *
     *     @OA\Parameter(name="id", in="path", @OA\Schema(type="integer")),
     *
     *     @OA\Response(response="204", description="Creates a new profile resource."
     * )
     *
     * Checks if a profile resource exists
     * and sets it as deleted.
     *
     * @see https://developer.mozilla.org/en-US/docs/Web/HTTP/Status/204
     *
     * @param  Request  $request
     */
    public function delete(int $id): string
    {
        Profile::findOrFail($id)->delete();
        return new JsonResponse(['Deleted item id' => $id], 204);
    }

    /**
     * Saves one or more fk items to the DB
     * 
     * @param array $profileAttributesArray
     * @param int $fk
     * @return void
     */
    public function createProfileAttributes(array $profileAttributesArray, int $fk): void
    {
        for ($i = 0; $i < count($profileAttributesArray); $i++) {
            $profileAttribute = new ProfileAttribute([
                'profile_id' => $fk,
                'attribute' => $profileAttributesArray[$i],
            ]);
            $profileAttribute->save();
        }
    }

    public function __construct()
    {
        parent::__construct();
    }
}
