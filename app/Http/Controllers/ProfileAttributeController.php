<?php

namespace App\Http\Controllers;

use App\Models\ProfileAttribute;
use Illuminate\Http\Request;

class ProfileAttributesController extends Controller
{
    /**
     * @see https://www.rfc-editor.org/rfc/rfc9110#media.type
     */
    private static string $CONTENT_TYPE = 'application/json; charset=UTF-8';

    /**
     * Checks for all the profile attributes filtering soft deleted items.
     *
     * @return ?string
     */
    public function showAllProfileAttributes(): ?string
    {
        return response()->json(ProfileAttribute::all(), 200, ['Content-Type' => self::$CONTENT_TYPE]);
    }

    /**
     * Checks for a single profile attribute resource by $id.
     */
    public function showProfileById(int $id): string
    {
        return response()->json(ProfileAttribute::find($id), 200, ['Content-Type' => self::$CONTENT_TYPE]);
    }

    /**
     * Creates a new profile attribute resource.
     */
    public function create(Request $request): string
    {
        $validated = $this->validate($request, [
            'attribute' => 'required|min:2|max:254|string',
        ]);

        $profileAttribute = ProfileAttribute::create($request->query());

        return response()->json($profileAttribute, 201);
    }

    /**
     * Checks if a profile attribute resource exists
     * and allows the resource to be updated.
     */
    public function update(int $id, Request $request): string
    {
        $profileAttribute = ProfileAttribute::findOrFail($id);
        $profileAttribute->update($request->query());

        return response()->json($profileAttribute, 200);
    }

    /**
     * Checks if a profile attribute resource exists
     * and sets it as deleted.
     *
     * @see https://developer.mozilla.org/en-US/docs/Web/HTTP/Status/204
     *
     * @param  Request  $request
     */
    public function delete(int $id): string
    {
        return response()->json(ProfileAttribute::findOrFail($id)->delete(), 204);
    }
}
