<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Monolog\Handler\StreamHandler;
use Monolog\Logger;

/**
 * Manages access and error log.
 */
class AccessLog
{
    /**
     * Handle an incoming request.
     */
    public function handle(Request $request, Closure $next): mixed
    {
        try {

            $response = $next($request);
            $this->logSuccess($request, $response);

        } catch (Exception $exception) {

            $this->logFailure($request, $exception);
            throw $exception;
        }

        try {
            return $next($request);
        } catch( Exception $e ) {
            return new JsonResponse('Sorry content negotiation went bad!', 406);
        }
    }

    /**
     * Logs the accesses to APIs.
     */
    protected function logSuccess(Request $request, mixed $response): void
    {
        $log = new Logger('access');
        $log->pushHandler(new StreamHandler('../storage/logs/access.log', 'NOTICE'));
        $log->notice($request->method().' request from '.$request->ip(), [
            'Requested URI' => $request->fullUrl(),
            'Payload' => $response->content()
        ]);
    }

    /**
     * Logs the errors in accessing API ressources.
     */
    protected function logFailure(Request $request, Exception $exception): void
    {
        $log = new Logger('error');
        $log->pushHandler(new StreamHandler('../storage/logs/error.log', 'WARNING'));
        $log->error($request->method().' EXCEPTION', [
            'Requested URI' => $request->fullUrl(),
            'Exception message' => $exception->getMessage() // 'Placeholder for $exception->getMessage()'
        ]);
    }
}
