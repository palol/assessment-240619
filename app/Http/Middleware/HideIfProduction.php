<?php

namespace App\Http\Middleware;

use Closure;

/**
 * Sets not authorized if in production env
 * e.g. switches the Laravel version message on root
 * TODO evolve to log-in to show API documentation
 */
class HideIfProduction
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     */
    public function handle($request, Closure $next): mixed
    {
        if (in_array(app()->environment(), ['prod', 'production', 'live'])) {
            return abort(403, 'You are not authorized to access this site.');
        }

        return $next($request);
    }
}
