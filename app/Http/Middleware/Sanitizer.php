<?php

namespace App\Http\Middleware;

use App\Services\Sanitation;
use Closure;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\ParameterBag;

/**
 * Description of Sanitizer
 */
class Sanitizer
{
    private Sanitation $s;

    /**
     * Check the parameters and clean according to type
     */
    private function cleanData(array $data): array
    {
        $s = $this->s;

        return collect($data)->map(function ($value, $key) use ($s) {
            return $s->sanitizeItem($key, $value);
        })->all();
    }

    /**
     * Clean the request's data.
     */
    private function clean(ParameterBag $bag): void
    {
        $bag->replace($this->cleanData($bag->all()));
    }

    /**
     * Handle an incoming request.
     */
    public function handle(Request $request, Closure $next): mixed
    {
        if (in_array($request->method(), ['POST', 'PUT'])) {
            if ($request->isJson()) {
                $this->clean($request->json());
            } else {
                $this->clean($request->query);
            }
        }

        try {
            return $next($request);
        } catch( Exception $e ) {
            return new JsonResponse('Sorry content negotiation went bad!', 406);
        }
    }

    public function __construct()
    {
        $this->s = new Sanitation();
    }
}
