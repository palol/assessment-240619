<?php

namespace App\Http;

use Illuminate\Http\Request;

/**
 * Description of BaseRequest
 *
 * @see https://hackernoon.com/always-return-json-with-laravel-api-870c46c5efb2
 */
final class BaseRequest extends Request
{
    public function expectsJson(): bool
    {
        return true;
    }

    public function wantsJson(): bool
    {
        return true;
    }
}
