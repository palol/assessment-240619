<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Prunable;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\SoftDeletes;

class ProfileAttribute extends Model implements SanitationableInterface
{
    use HasFactory, Prunable, SoftDeletes; //HasApiTokens, HasFactory, Notifiable

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'profile_id',
        'attribute',
        'updated_at',
        'deleted',
    ];

    /**
     * The attributes excluded from the model's client interaction.
     *
     * @var array
     */
    protected $hidden = ['created_at'];

    /**
     * Field - Type map
     *
     * @var array
     */
    protected $cast = [
        'attribute' => 'string',
    ];

    /**
     * Get field type map
     */
    public function getCast(): array
    {
        return $this->cast;
    }

    /**
     * Defines reverse relationship.
     *
     * @return Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function profile(): BelongsTo
    {
        return $this->belongsTo(Profile::class);
    }

    public function setProfileAttribute(string $a): void
    {
        $this->attributes['attribute'] = $a;
    }
}
