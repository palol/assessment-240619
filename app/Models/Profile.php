<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Casts\AsArrayObject;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Prunable;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\SoftDeletes;

class Profile extends Model implements SanitationableInterface
{
    use HasFactory, Prunable, SoftDeletes; //HasApiTokens, Notifiable

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name', 'last_name', 'telephone', 'profile_attributes', 'updated_at', 'deleted',
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = ['created_at'];

    /**
     * Field - Type map
     *
     * @var array
     */
    protected $cast = [
        'first_name' => 'string',
        'last_name' => 'string',
        'telephone' => 'string',
        'profile_attributes' => AsArrayObject::class,
    ];

    /**
     * Get field type map
     */
    public function getCast(): array
    {
        return $this->cast;
    }

    /**
     * Defines relationship.
     *
     * @return Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function profileAttributes(): HasMany
    {
        return $this->hasMany(ProfileAttribute::class);
    }

    /**
     * Set the profile first name.
     */
    public function setFirstNameAttribute(string $firstName): void
    {
        $this->attributes['first_name'] = $firstName;
    }

    /**
     * Set the profile last name.
     */
    public function setLastNameAttribute(string $lastName): void
    {
        $this->attributes['last_name'] = $lastName;
    }

    /**
     * Set the profile telephone.
     */
    public function setTelephoneAttribute(string $telephone): void
    {
        $this->attributes['telephone'] = $telephone;
    }

    /**
     * Sets a profile attribute
     * 
     * @param ProfileAttribute $pa
     * @return void
     */
    public function setProfileAttribute(ProfileAttribute $pa): void
    {
        $this->attributes['profile_attributes'].push($pa);
    }
}
