<?php

namespace App\Models;

/**
 * Implements the required methods for Model attribute sanitation
 */
interface SanitationableInterface
{
    public function getCast(): array;
}
