<?php

/** @var \Laravel\Lumen\Routing\Router $router */

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', fn () => $router->app->version());

/**
 * Defines the HTTP routes for the Profile Model
 */
$router->group(['prefix' => 'api'], function () use ($router) { //'json-type-setter', 'middleware' => ['log']

    $router->get('profiles', [
        'uses' => 'ProfileController@showAllProfiles',
    ]);

    $router->get('profile/{id}', [
        'uses' => 'ProfileController@showProfileById',
    ]);

    $router->post('profile', [
        'uses' => 'ProfileController@create',
    ]);

    $router->delete('profile/{id}', [
        'uses' => 'ProfileController@delete',
    ]);

    $router->put('profile/{id}', [
        'uses' => 'ProfileController@update',
    ]);
});
