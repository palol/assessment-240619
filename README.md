# A CRUD API with Lumen 9

Build a containerized Lumen 9 app with any relational DB
and PHP version > 8.0.

## Tasks
...

## Implementation diary

This project was first developed on a LAMP stack with [devilbox](http://devilbox.org/)
and then bundled in a container.

## Personal wishes
* API documentation with: https://medium.com/@tatianaensslin/how-to-add-swagger-ui-to-php-server-code-f1610c01dc03
* Codebase documentation with: https://phpdoc.org/
* Implementation based on https://learn.openapis.org/

## Install and run the app

### Requirements
* Git or something to download the project from GitLab.
* Docker CE or Docker desktop.
    * In order to start the provided container image run `docker compose up --build`. `Ctrl + c` to exit.
* Or a (L)AMP or (L)EMP stack, the project was developed on a LAMP stack with [devilbox](http://devilbox.org/).
    * Required [PHP extensions](https://laravel.com/docs/10.x/deployment#server-requirements) (at the time of writing all required PHP modules are already installed in PHP 8.1 standard package):
        * Ctype PHP Extension
        * cURL PHP Extension
        * DOM PHP Extension
        * Fileinfo PHP Extension
        * Filter PHP Extension
        * Hash PHP Extension
        * Mbstring PHP Extension
        * OpenSSL PHP Extension
        * PCRE PHP Extension
        * PDO PHP Extension
        * Session PHP Extension
        * Tokenizer PHP Extension
        * XML PHP Extension
    * Development configuration (this may vary in the future according to the default versions in the used images listed in the Dockerfile and compose.yml):
        * Apache 2.4
        * PHP 8.1.13
        * MariaDB 10.6-0.21

### Installing

```
git clone https://gitlab.com/palol/assessment-240619.git
cd assessment-240619
docker compose up --build
```

### Running

Load the configured routes (if you are using  [devilbox](http://devilbox.org/), 
according to the exposed URL on your `/etc/hosts` server) 
in your desired client app (e.g. CURL, Browser, Postman, Apidog, Swagger UI, whatever, ...).  

The container is inspectable with `docker exec -ti <container_hash> bash`.

## Configured services


### Configured docker services

* API root `http://localhost:9000/`
    * In order to enable the POST, PUT and DELETE endpoints you have to send the token, that is available in the table users.api_token, in the header of the request. Key: `Api-Token`, value: the value of the database table field `users.api_token`.
* PHPMyAdmin `http://localhost:8080/`
* Adminer `http://localhost:8081/`
* MariaDB reachable within the docker network at port `3306`

### Configured composer scripts

* `composer document`: creates openapi.yaml,
* `composer fix-permissions`: Updates files permission,
* `composer lint`: runs static code fixing,
* `composer test`: runs the configured tests,
* `composer sniff`: runs static code analisys.

## Sources and credits

### Bash

* https://superuser.com/questions/508507/linux-bash-script-single-command-but-multiple-lines

### Docker

* https://docs.docker.com/language/php/containerize/
* Docker in Windows: https://www.pascallandau.com/blog/php-php-fpm-and-nginx-on-docker-in-windows-10/
* https://forums.docker.com/t/docker-compose-yml-version-is-obsolete/141313
* Search Laravel API in docker hub: https://hub.docker.com/_/kong
* `docker pull laravelphp/vapor:php81`
* https://hub.docker.com/_/api-firewall
* https://hub.docker.com/r/bitnami/kubeapps-apis
* https://hub.docker.com/r/mattrayner/lamp
* https://hub.docker.com/r/fauria/lamp/
* https://dev.to/bornfightcompany/lamp-docker-setup-with-php-8-and-mariadb-for-symfony-projects-5hic
* https://stackoverflow.com/questions/30639174/how-to-set-up-file-permissions-for-laravel
* (setting) `hostname` needs SYS_ADMIN capabilities to work. Docker for security reasons disables SYS_ADMIN capabilities: https://man7.org/linux/man-pages/man2/sethostname.2.html
* https://www.digitalocean.com/community/tutorials/how-to-set-up-laravel-nginx-and-mysql-with-docker-compose

### Apache 2.4
* `apachectl -M` Lists enabled modules.
* `a2enmod <module>` enables the desired module.
* `update-alternatives --config www-browser` set default command line browser (required for `apachectl status`).

### Laravel
* https://artisan.page/9.x
* https://stackoverflow.com/questions/40702739/laravel-reset-auto-increment-before-re-seeding-a-table
* https://laravel.com/docs/5.1/eloquent#querying-soft-deleted-models
* https://stackoverflow.com/questions/26437342/laravel-migration-best-way-to-add-foreign-key
* https://geekflare.com/laravel-eloquent-model-relationship/
* https://laravel.com/docs/11.x/eloquent-relationships#querying-belongs-to-relationships
* https://stackoverflow.com/questions/36366727/how-do-you-force-a-json-response-on-every-response-in-laravel seems to lack app/kernel to work
* https://beyondco.de/blog/a-guide-to-soft-delete-models-in-laravel
* https://laravel.com/docs/9.x/validation
* Nice! https://github.com/stuyam/laravel-phone-validator
* Regex https://stackoverflow.com/questions/16699007/regular-expression-to-match-standard-10-digit-phone-number
* https://stackoverflow.com/questions/28754920/validate-field-input-is-string-laravel
* https://laravel.com/docs/9.x/eloquent-mutators#defining-a-mutator (a.k.a. Setters)
* https://stackoverflow.com/questions/34820042/how-can-i-sanitize-laravel-request-inputs
* GET results pagination: https://laravel.com/docs/9.x/pagination
* [Laravel best practices about naming conventions](https://github.com/alexeymezenin/laravel-best-practices#follow-laravel-naming-conventions)

### Lumen
* https://github.com/laravel/lumen/tree/9.x
* https://auth0.com/blog/developing-restful-apis-with-lumen/
* https://laracasts.com/discuss/channels/lumen/lumen-how-to-add-make-or-generate-artisan-commands-for-model-controller-etc
* https://packagist.org/packages/orumad/lumen-config-cache is not installable with the current set of packages
* https://lumen.laravel.com/docs/9.x/responses seems not to set proper content type
* https://lumen.laravel.com/docs/9.x/middleware
* According to https://laracasts.com/discuss/channels/lumen/caching-routesenvconfig-in-lumen Lumen does not even look for cached config or routes files
* https://lumen.laravel.com/docs/9.x/validation
* https://code.tutsplus.com/how-to-secure-a-rest-api-with-lumen--cms-27442t
* Sanctum is supported in Laravel only https://laracasts.com/discuss/channels/lumen/use-laravel-sanctum-in-lumen use https://github.com/tymondesigns/jwt-auth instead.

### MySQL
* https://www.mysqltutorial.org/mysql-basics/mysql-boolean/
* https://stackoverflow.com/questions/289727/which-mysql-data-type-to-use-for-storing-boolean-values although the advised data type should be BOOLEAN, I choosed the TINYINT(1) because it is used by PHPMyAdmin in SHOW CREATE TABLE profiles.

### PHP
* https://stackoverflow.com/questions/1957629/how-to-access-a-private-member-inside-a-static-function-in-php

### Testing
* https://docs.phpunit.de/en/9.6/
* https://medium.com/the-andela-way/mock-testing-in-laravel-4a2fe15885b8
* https://laravel.com/api/8.x/Illuminate/Testing/AssertableJsonString.html
* https://medium.com/@currypat1985/one-to-many-rest-api-with-lumen-98464b8c6f42

### tmp
a541fe0798f4fe33d3919af235e3bdde34635d03 refs/tags/v0.1.0
41c86afa135a36fbda42d4d85d25e99fa364eacf refs/tags/v0.1.1
f3d14312b9ecaeae7390d03da6cc4ad1f16449f7 refs/tags/v0.1.2
bbcb75f40f6087abf1ffffc3ea9756d306e2e10e refs/tags/v0.1.3
f291688166b68f1a541b4a3cb2480ef3e2014f9b refs/tags/v0.1.4
??
5eaf0079ff9a9c38e3cbde6c3ca0cbb6b63bc430

## Changelog
- v0.1.0 Bringing the APIs to life.
- v0.1.1 GET method is working for listing profiles.
- v0.1.2 APIs working without authentication for profiles, handling soft deletion, logging seems to function on a daily basis.
- v0.1.3 The APIs work with Eloquent queries only and with default Laravel structure for soft deletion. Three field validation for Profiles and First Test.
- v0.1.4 Create, update and delete are triggered only if Api-Token is present in header.
- v0.1.5 Sanitizer in middleware and logging access
- v0.1.6 Bringing containers to life.
- v0.1.7 Fixed all major bugs in API routes.
