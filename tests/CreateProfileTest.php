<?php

namespace Tests;

use App\Models\Profile;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;

final class CreateProfileTest extends TestCase
{
    /**
     * @see https://laravel.com/docs/9.x/database-testing
     */
   // use RefreshDatabase;
    
    protected function setUp(): void 
    {
        if (defined('API_TOKEN')) {
           /* $user = User::factory()->create();
            
            die();*/
            define('API_TOKEN', '7575456e-4c53-3ecf-8f53-d3c8d6c17454');       
        }
    }
    /**
     * Testing the required flag for the first_name
     */
    public function testOnCreateFirstNameRequired(): void
    {
        $profile = Profile::factory()->make();
        $profile->setFirstNameAttribute(' ');

        $this->post('/api/profile', json_decode(json_encode($profile), ['Api-Token' => API_TOKEN]));
        $response = $this->response->getContent();

        $this->assertJson($response);
        $this->assertStringContainsString('"first_name":["The first name field is required."', $response, 'String not found!');
        // !! Faker produces telephone numbers that not always comply with the validator regex invalidating the test
        //$this->assertJsonStringEqualsJsonString('{"first_name":["The first name field is required."]}', $response, 'String not found!');
    }

    /**
     * Testing the required flag for the last_name
     */
    public function testOnCreateLastNameRequired(): void
    {
        $profile = Profile::factory()->make();
        $profile->setLastNameAttribute(' ');

        $this->post('/api/profile', json_decode(json_encode($profile), ['Api-Token' => API_TOKEN]));
        $response = $this->response->getContent();

        $this->assertJson($response);
        $this->assertStringContainsString('"last_name":["The last name field is required."', $response, 'String not found!');
        // !! Faker produces telephone numbers that not always comply with the validator regex invalidating the test
        //$this->assertJsonStringEqualsJsonString('{"last_name":["The last name field is required."]}', $response, 'String not found!');
    }

    /**
     * Testing the required flag for the telephone
     */
    public function testOnCreateTelephoneRequired(): void
    {
        $profile = Profile::factory()->make();
        $profile->setTelephoneAttribute(' ');

        $this->post('/api/profile', json_decode(json_encode($profile), ['Api-Token' => API_TOKEN]));
        $response = $this->response->getContent();

        $this->assertJson($response);
        $this->assertJsonStringEqualsJsonString('{"telephone":["The telephone field is required."]}', $response, 'String not found!');
    }
}
