#!/bin/bash
echo ""
echo "Give the executing environment case sensitive."
echo "Environment is one of production|local|testing."
echo ""
read ENVIRONMENT
rm -f .env
cp -p .env.$ENVIRONMENT .env
echo ""
echo "Environment set."
exit 0;

