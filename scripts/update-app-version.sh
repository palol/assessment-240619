#!/bin/bash 
CURRENT_BRANCH="${git rev-parse --abbrev-ref HEAD}"
echo ""
echo "Last saved git tag is: "$(git describe --tags --abbrev=0)
echo ""
echo "Give the next version number (e.g. %d.%d.%d)."
echo ""
read VERSION
echo ""
echo "Write a description."
echo ""
read MESSAGE

git tag -a v$VERSION -m "$MESSAGE"

#.env > sed 's/^\(APP_VERSION=\).*$/APP_VERSION='$VERSION'/' .env 
#sed 's/^\(APP_VERSION=\).*$/APP_VERSION='$VERSION'/' .env.example > .env.example
#echo "${sed 's/^\(APP_VERSION=\).*$/APP_VERSION='$VERSION'/' .env.local}" > .env.local
#sed 's/^\(APP_VERSION=\).*$/APP_VERSION='$VERSION'/' .env.production > .env.production
#sed 's/^\(APP_VERSION=\).*$/APP_VERSION='$VERSION'/' .env.testing > .env.testing
 
echo "- v$VERSION $MESSAGE" >> README.md
git commit -am "Appended version v$VERSION in README"
git push origin $CURRENT_BRANCH
exit 0
