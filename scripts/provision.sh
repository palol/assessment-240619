#!/bin/bash

# Assuming .env is local https://devilbox-test.readthedocs.io
# TODO read data from .env
USER="root"
PASSWORD=""
DATABASE="assessment240619"
DBHOST="127.0.0.1"

# Creates database assuming 
mysql -h $DBHOST --user="$USER" --password="$PASSWORD" --execute="DROP DATABASE IF EXISTS $DATABASE; CREATE DATABASE $DATABASE;"

php artisan migrate:install

# Creates table profiles
# mysql -h 127.0.0.1 --user="root" --password="" --database="assessment240619" --execute="CREATE TABLE profiles ( id int(10) unsigned NOT NULL AUTO_INCREMENT, first_name varchar(254) NOT NULL, last_name varchar(254) NOT NULL, telephone varchar(16) NOT NULL, created_at datetime NOT NULL DEFAULT current_timestamp(), updated_at datetime NOT NULL DEFAULT current_timestamp(), deleted boolean NOT NULL DEFAULT=0, PRIMARY KEY (id) ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4"
mysql -h $DBHOST --user="$USER" --password="$PASSWORD" --database="$DATABASE" --execute="CREATE TABLE `profiles` ( \
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT, \
  `first_name` varchar(254) NOT NULL, \
  `last_name` varchar(254) NOT NULL, \
  `telephone` varchar(25) NOT NULL, \
  `created_at` datetime NOT NULL DEFAULT current_timestamp(), \
  `updated_at` datetime NOT NULL DEFAULT current_timestamp(), \
  `deleted` boolean NOT NULL DEFAULT=0, \
  PRIMARY KEY (`id`) \
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4"

#CREATE TABLE `profiles` (
#  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
#  `first_name` varchar(254) NOT NULL,
#  `last_name` varchar(254) NOT NULL,
#  `telephone` varchar(25) NOT NULL,
#  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
#  `updated_at` datetime NOT NULL DEFAULT current_timestamp(),
#  `deleted` tinyint(1) NOT NULL DEFAULT 0,
#  PRIMARY KEY (`id`)
#) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci

# Creates table profile_attributes
mysql -h $DBHOST --user="$USER" --password="$PASSWORD" --database="$DATABASE" --execute="CREATE TABLE `profile_attributes` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `profile_id` int(10) unsigned NOT NULL,
  `attribute` varchar(254) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_at` datetime NOT NULL DEFAULT current_timestamp(),
  `deleted` boolean NOT NULL DEFAULT=0, \
  PRIMARY KEY (`id`),
  KEY `fk_profile_id` (`profile_id`),
  CONSTRAINT `fk_profile_id` FOREIGN KEY (`profile_id`) REFERENCES `profiles` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4"


# ALTER TABLE profile_attributes ADD CONSTRAINT fk_profile_id FOREIGN KEY (profile_id) REFERENCES profiles(id); 

# Create eloquent models
php artisan make:model -q -n Profiles
php artisan make:model -q -n ProfileAttributes

# Create Seeder classes
php artisan make:seeder -q -n ProfileSeeder
php artisan make:seeder -q -n ProfileAttributeSeeder

# Seeds DB
php artisan db:seed

# Deletes example and creates controllers
rm -f ./app/Http/ExampleController.php
php artisan make:controller Profiles
php artisan make:controller --parent Profiles ProfileAttributes

# Sets daily log rotation
# TODO awk LOG_CHANNEL to daily
php artisan config:clear && php artisan cache:clear && php artisan config:cache

