#!/bin/bash 
composer install

php artisan migrate:fresh
php artisan db:seed

echo ""
echo "Enjoy!"
echo ""

exit 0;
