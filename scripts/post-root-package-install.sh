#!/bin/bash
#
# post-root-package-install.sh
# creates a symlink to public if it isnt already there
# creates the .env files
# sets the APP_KEY in .env
#

if [ ! -L "htdocs" ]; then
  ln -s ./public ./htdocs
fi

if [ ! -f ".env.production" ]; then
    cp -p .env.example .env.production
fi

if [ ! -f ".env.local" ]; then
    cp -p .env.example .env.local
fi

if [ ! -f ".env.testing" ]; then
    cp -p .env.example .env.testing
fi

if [ ! -f ".env" ]; then
    cp -p .env.local .env
fi

php artisan key:generate

echo ""
echo "IMPORTANT! Configure all .env* files with real data."
echo ""

# TODO add parameter to confirm .env.example was 
# updated with real values in order to seed DB 
# 
# php artisan db:seed

exit 0;

