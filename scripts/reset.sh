#!/bin/bash

rm -rf vendor/* composer.lock #node_modules/* package-lock.json
composer install #&& pnpm install

php artisan migrate:fresh
php artisan db:seed

php artisan cache:clear

exit 0;
