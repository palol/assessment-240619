#!/bin/bash

CURRENT_BRANCH="$(git rev-parse --abbrev-ref HEAD)"
BKP_BRANCH="bkp-$(date +%s)-$CURRENT_BRANCH"
INCONSISTENT="$(git status --short | wc -l)"

if [ $INCONSISTENT -gt 0 ]; then
  echo ""
  echo "Uncommitted work is present!"
  echo ""
  git status
  echo ""
  echo "...adding and committing changes."
  git add .
  git commit -am "Snapshot before hard resetting"
fi

echo ""
echo "Current working branch is $CURRENT_BRANCH"
echo "ALERT! If you go on $CURRENT_BRANCH is going to be overwritten by the origin/$CURRENT_BRANCH"
echo ""

echo "Current situation:"
git branch -avv
echo ""
echo "Do you want to hard reset? (y/n)"
read GO_ON
echo ""
if [ $GO_ON = "y" ] || [ $GO_ON = "Y"] || [ $GO_ON = "yes"] || [ $GO_ON = "YES"]; then
  git checkout -b $BKP_BRANCH
  git checkout $CURRENT_BRANCH
  echo "Backed-up $CURRENT_BRANCH to $BKP_BRANCH"
  echo "...hard resetting $CURRENT_BRANCH to origin/$CURRENT_BRANCH"
  git fetch --all
  git reset --hard origin/$CURRENT_BRANCH
else
  echo ""
  echo "Exiting script! Nothing to do."
  echo ""
  exit 1;
fi

exit 0;
