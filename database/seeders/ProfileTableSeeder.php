<?php

namespace Database\Seeders;

use DB;
use Faker;
use Illuminate\Database\Seeder;

class ProfileTableSeeder extends Seeder implements TransactionableInterface
{
    /**
     * The table name that reference current seeder
     */
    private static string $TABLE = 'profiles';

    public function beginTransaction(): void
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0');
        DB::table(self::$TABLE)->truncate();
        DB::statement('ALTER TABLE '.self::$TABLE.' AUTO_INCREMENT=1');
    }

    public function endTransaction(): void
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=1');
    }

    /**
     * Run the database seeds.
     *
     * @see vendor/fzaninotto/faker/src/Faker/Generator.php
     */
    public function run(): void
    {
        $faker = Faker\Factory::create();

        $this->beginTransaction();

        DB::table(self::$TABLE)->insert([
            'first_name' => $faker->firstName(),
            'last_name' => $faker->lastName,
            'telephone' => $faker->phoneNumber,
            'created_at' => date('Y-m-d H:i:s'),
        ]);

        DB::table(self::$TABLE)->insert([
            'first_name' => $faker->firstName(),
            'last_name' => $faker->lastName,
            'telephone' => $faker->phoneNumber,
            'created_at' => date('Y-m-d H:i:s'),
        ]);

        DB::table(self::$TABLE)->insert([
            'first_name' => $faker->firstName(),
            'last_name' => $faker->lastName,
            'telephone' => $faker->phoneNumber,
            'created_at' => date('Y-m-d H:i:s'),
        ]);

        $this->endTransaction();
    }
}
