<?php

namespace Database\Seeders;

use DB;
use Faker;
use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * The table name that reference current seeder
     */
    private static string $TABLE = 'users';

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();

        DB::table(self::$TABLE)->insert([
            'name' => $faker->domainWord,
            'email' => $faker->email,
            'password' => $faker->password(),
            'device' => $faker->macAddress,
            'api_token' => $faker->uuid,
            'expires' => date('Y-m-d H:i:s', strtotime(' +2 months')),
            'created_at' => date('Y-m-d H:i:s'),
        ]);
    }
}
