<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     */
    public function run(): void
    {
        $this->call(ProfileTableSeeder::class);
        $this->command->info('Profile table (profiles) seeded!');

        $this->call(ProfileAttributeTableSeeder::class);
        $this->command->info('ProfileAttribute table (profile_attributes) seeded!');

        $this->call(UserTableSeeder::class);
        $this->command->info('Subscriber table (subscriber) seeded!');
    }
}
