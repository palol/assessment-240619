<?php

namespace Database\Seeders;

/**
 * TransactionableInterface contract handles the foreign key
 * constraint and auto increment set as well as other
 * functionalities to be set at beginning and at the end
 * of a transaction.
 *
 * @author palu
 */
interface TransactionableInterface
{
    public function beginTransaction(): void;

    public function endTransaction(): void;
}
