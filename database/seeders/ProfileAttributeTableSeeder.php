<?php

namespace Database\Seeders;

use DB;
use Faker;
use Illuminate\Database\Seeder;

class ProfileAttributeTableSeeder extends Seeder implements TransactionableInterface
{
    /**
     * The table name that reference current seeder
     */
    private static string $TABLE = 'profile_attributes';

    public function beginTransaction(): void
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0');
        DB::table(self::$TABLE)->truncate();
        DB::statement('ALTER TABLE '.self::$TABLE.' AUTO_INCREMENT=1');
    }

    public function endTransaction(): void
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=1');
    }

    /**
     * Run the database seeds.
     * TODO handle foreign key.s
     *
     * @see vendor/fzaninotto/faker/src/Faker/Generator.php
     */
    public function run(): void
    {
        $faker = Faker\Factory::create();

        $this->beginTransaction();

        DB::table(self::$TABLE)->insert([
            'profile_id' => 1,
            'attribute' => 'Attribute 1 from seeder: '.implode(' ', $faker->words()),
            'created_at' => date('Y-m-d H:i:s'),
        ]);

        DB::table(self::$TABLE)->insert([
            'profile_id' => 1,
            'attribute' => 'Attribute 2 from seeder: '.implode(' ', $faker->words()),
            'created_at' => date('Y-m-d H:i:s'),
        ]);

        $this->endTransaction();
    }
}
