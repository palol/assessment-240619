<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('profile_attributes', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('profile_id')
                ->unsigned()
                ->index()
                ->nullable();
            $table->foreign('profile_id')
                ->references('id')->on('profiles')
                ->cascadeOnUpdate()
                ->nullOnDelete();
            $table->string('attribute', 254);
            $table->softDeletes();
            //$table->boolean('deleted')->default(false);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('profile_attributes');
    }
};
