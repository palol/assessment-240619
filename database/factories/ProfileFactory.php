<?php

namespace Database\Factories;

use App\Models\Profile;
use Faker;
use Illuminate\Database\Eloquent\Factories\Factory;

class ProfileFactory extends Factory
{
    protected $profiles = Profile::class;

    public function definition(): array
    {
        $faker = Faker\Factory::create();

        return [
            'first_name' => $faker->firstName(),
            'last_name' => $faker->lastName,
            'telephone' => $faker->phoneNumber,
            'created_at' => date('Y-m-d H:i:s'),
        ];
    }
}
