<?php

namespace Database\Factories;

use App\Models\ProfileAttribute;
use Faker;
use Illuminate\Database\Eloquent\Factories\Factory;

class ProfileAttributesFactory extends Factory
{
    protected $profileAttributes = ProfileAttribute::class;

    public function definition(): array
    {
        $faker = Faker\Factory::create();

        return [
            'profile_id' => 1,
            'attribute' => 'Attribute 1 from seeder: '.implode(' ', $faker->words()),
            'created_at' => date('Y-m-d H:i:s'),
        ];
    }
}
