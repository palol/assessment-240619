<?php

namespace Database\Factories;

use App\Models\User;
use Faker;
use Illuminate\Database\Eloquent\Factories\Factory;

class UserFactory extends Factory
{
    protected $user = User::class;

    public function definition(): array
    {
        $faker = Faker\Factory::create();

        return [
            'name' => $faker->domainWord,
            'email' => $faker->email,
            'password' => $faker->password,
            'device' => $faker->macAddress,
            'api_token' => $faker->uuid,
            'expires' => date('Y-m-d H:i:s', strtotime(' +2 months')),
            'created_at' => date('Y-m-d H:i:s'),
        ];
    }
}
